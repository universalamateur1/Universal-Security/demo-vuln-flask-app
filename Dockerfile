FROM python:3.9

RUN apt-get -y update

COPY app /
COPY requirements.txt /app/requirements.txt

WORKDIR /app
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

ENTRYPOINT [ "python" ]
CMD [ "/app.py" ]
